Pod::Spec.new do |s|
  s.name             = "IDDataSource"
  s.version          = "0.1.0"
  s.summary          = "Abstract protocol that mimics NSFetchedResultsController."
  s.description      = <<-DESC
                       Abstract protocol that mimics NSFetchedResultsController with a default implementation
                       DESC
  s.homepage         = "https://bitbucket.org/i-dama/iddatasource"
  s.license          = 'MIT'
  s.author           = { "Ivan Damjanović" => "ivan.damjanovic@infinum.hr" }
  s.source           = { :git => "https://bitbucket.org/i-dama/iddatasource.git", :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/damaofficial'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
end
