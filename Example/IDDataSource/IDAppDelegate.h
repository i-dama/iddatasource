//
//  IDAppDelegate.h
//  IDDataSource
//
//  Created by CocoaPods on 05/22/2015.
//  Copyright (c) 2014 Ivan Damjanović. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
