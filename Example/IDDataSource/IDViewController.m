//
//  IDViewController.m
//  IDDataSource
//
//  Created by Ivan Damjanović on 05/22/2015.
//  Copyright (c) 2014 Ivan Damjanović. All rights reserved.
//

#import "IDViewController.h"
#import <IDDataSource/IDDataSource.h>
#import <IDDataSource/IDPlainDataSource.h>
#import <IDDataSource/NSMutableArray+IDDataSource.h>
#import <IDDataSource/IDTableViewUpdater.h>

@interface IDViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) id<IDDataSource> dataSource;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation IDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
//    self.dataSource = [IDPlainDataSource newWithItems:@[
//                                                        @"This",
//                                                        @"Is",
//                                                        @"An",
//                                                        @"Example",
//                                                        @"Of",
//                                                        @"A",
//                                                        @"Plain",
//                                                        @"Data",
//                                                        @"Source",
//                                                        @"Containing",
//                                                        @"Only",
//                                                        @"Strings"
//                                                        ]];
    NSMutableArray *array = [NSMutableArray arrayWithObjects:@"Mutable", @"Example", nil];
    self.dataSource = array;
    [IDTableViewUpdater updateTableView:self.tableView onChangesFromDataSource:self.dataSource withDelegate:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [array ID_addObject:@"Freshly added!"];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dataSource numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSource numberOfItemsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSString *text = [self.dataSource itemAtIndexPath:indexPath];
    cell.textLabel.text = text;
    
    return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Data source delegate


@end
