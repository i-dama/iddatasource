//
//  NSMutableArray+IDDataSource.m
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import "NSMutableArray+IDDataSource.h"
#import <UIKit/UIKit.h>

@implementation NSMutableArray (IDDataSource)

#pragma mark - Override

- (void)ID_addObject:(id)anObject
{
    [self addObject:anObject];
    if ([self.delegate respondsToSelector:@selector(dataSourceWillChangeContent:)]) {
        [self.delegate dataSourceWillChangeContent:self];
    }
    if ([self.delegate respondsToSelector:@selector(dataSource:didChangeObject:atIndexPath:forChangeType:newIndexPath:)]) {
        [self.delegate dataSource:self didChangeObject:anObject atIndexPath:nil forChangeType:IDDataSourceChangeInsert newIndexPath:[NSIndexPath indexPathForRow:self.count-1 inSection:0]];
    }
    if ([self.delegate respondsToSelector:@selector(dataSourceDidChangeContent:)]) {
        [self.delegate dataSourceDidChangeContent:self];
    }
}

@end
