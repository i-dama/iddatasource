//
//  IDPlainSection.m
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import "IDPlainSection.h"

@interface IDPlainSection ()
@property (strong, nonatomic) id sectionInfo;
@property (strong, nonatomic) NSArray *items;
@end

@implementation IDPlainSection

+ (instancetype)newWithSectionInfo:(id)sectionInfo items:(NSArray *)items
{
    IDPlainSection *plainSection = [IDPlainSection new];
    plainSection.sectionInfo = sectionInfo;
    plainSection.items = items;
    return plainSection;
}

- (NSInteger)numberOfItems
{
    return self.items.count;
}

- (id)itemAtIndex:(NSInteger)index
{
    return [self.items objectAtIndex:index];
}

- (NSInteger)indexForItem:(id)item
{
    return [self.items indexOfObject:item];
}

@end
