//
//  IDPlainDataSource.m
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import "IDPlainDataSource.h"
#import <UIKit/UIKit.h>

@interface IDPlainDataSource ()

@property (strong, nonatomic) NSArray *sections;

@end

@implementation IDPlainDataSource
//not actually used yet.
@synthesize delegate;


+ (instancetype)newWithSections:(NSArray *)sections
{
    IDPlainDataSource *dataSource = [IDPlainDataSource new];
    dataSource.sections = sections;
    return dataSource;
}

+ (instancetype)newWithItems:(NSArray *)items
{
    return [self newWithSections:@[[IDPlainSection newWithSectionInfo:nil items:items]]];
}

#pragma mark - Data source methods

- (NSInteger)numberOfSections
{
    return self.sections.count;
}

- (id)sectionInfoForSection:(NSInteger)sectionIndex
{
    IDPlainSection *section = [self.sections objectAtIndex:sectionIndex];
    return section.sectionInfo;
}

- (NSInteger)numberOfItemsInSection:(NSInteger)sectionIndex
{
    IDPlainSection *section = [self.sections objectAtIndex:sectionIndex];
    return [section numberOfItems];
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    IDPlainSection *section = [self.sections objectAtIndex:indexPath.section];
    return [section itemAtIndex:indexPath.row];
}

- (NSIndexPath *)indexPathForItem:(id)item
{
    for (NSInteger sectionIndex = 0; sectionIndex < self.sections.count; sectionIndex ++) {
        IDPlainSection *section = [self.sections objectAtIndex:sectionIndex];
        NSInteger index = [section indexForItem:item];
        if (index != NSNotFound) {
            return [NSIndexPath indexPathForRow:index inSection:sectionIndex];
        }
    }
    return nil;
}

@end
