//
//  IDPlainDataSource.h
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import <Foundation/Foundation.h>
#import "IDDataSource.h"
#import "IDPlainSection.h"

/**
 Read only imlpementation of IDDataSource
 */
@interface IDPlainDataSource : NSObject <IDDataSource>

/**
 Array of IDPlainSection objects
 */
+ (instancetype)newWithSections:(NSArray *)sections;

/**
 Creates a data source with a single section containing the items with no section info
 */
+ (instancetype)newWithItems:(NSArray *)items;

@end
