//
//  IDPlainSection.h
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import <Foundation/Foundation.h>

@interface IDPlainSection : NSObject
/**
 Should be self explanatory
 */
+ (instancetype)newWithSectionInfo:(id)sectionInfo items:(NSArray *)items;

@property (readonly, strong, nonatomic) id sectionInfo;

- (NSInteger)numberOfItems;
- (id)itemAtIndex:(NSInteger)index;

- (NSInteger)indexForItem:(id)item;

@end
