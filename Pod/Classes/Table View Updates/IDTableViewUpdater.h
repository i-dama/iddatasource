//
//  IDTableViewUpdater.h
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import <Foundation/Foundation.h>
#import "IDDataSource.h"
#import <UIKit/UIKit.h>

@protocol IDTableViewUpdaterDelegate;


/**
 Contains all of the boilerplate code for updating table views when the data source changes
 */
@interface IDTableViewUpdater : NSObject

/**
 Tracks the changes from the data source and updates the table view accordingly.
 Updates will be sent to the table view as long as the data source is not deallocated
 If you want to perform custom reloads, pass in the delegate. If the delegate is not
 passed, the updated cell will be reloaded
 */
+ (void)updateTableView:(UITableView *)tableView onChangesFromDataSource:(id<IDDataSource>)dataSource withDelegate:(id<IDTableViewUpdaterDelegate>)delegate;

@end

@protocol IDTableViewUpdaterDelegate <NSObject>

@required
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end