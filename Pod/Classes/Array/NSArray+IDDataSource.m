//
//  NSArray+IDDataSource.m
//  Pods
//
//  Created by Dama on 29/05/15.
//
//

#import "NSArray+IDDataSource.h"
#import <objc/runtime.h>

static char delegateKey;

@implementation NSArray (IDDataSource)
@dynamic delegate;

#pragma mark - Delegate

- (void)setDelegate:(id<IDDataSourceDelegate>)delegate
{
    objc_setAssociatedObject(self, &delegateKey, delegate, OBJC_ASSOCIATION_ASSIGN);
}

- (id<IDDataSourceDelegate>)delegate
{
    return objc_getAssociatedObject(self, &delegateKey);
}

#pragma mark - Data source

- (NSInteger)numberOfSections
{
    return 1;
}

- (id)sectionInfoForSection:(NSInteger)sectionIndex
{
    return nil;
}

- (NSInteger)numberOfItemsInSection:(NSInteger)sectionIndex
{
    return self.count;
}

- (id)itemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self objectAtIndex:indexPath.row];
}

- (NSIndexPath *)indexPathForItem:(id)item
{
    NSInteger index = [self indexOfObject:item];
    if (index == NSNotFound) {
        return nil;
    } else {
        return [NSIndexPath indexPathForRow:index inSection:0];
    }
}

@end
