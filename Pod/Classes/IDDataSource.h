//
//  IDDataSource.h
//  Pods
//
//  Created by Dama on 22/05/15.
//
//

#import <Foundation/Foundation.h>

/**
 An protocol representing a data source with sections and items.
 */

@protocol IDDataSourceDelegate;

@protocol IDDataSource <NSObject>

/**
 The delegate for the data source.
 The delegate is notified of all of the data source changes
 */
@property (weak, nonatomic) id<IDDataSourceDelegate> delegate;

/**
 Returns the number of sections in the data source.
 */
- (NSInteger)numberOfSections;

/**
 Retrns the number of items in the given section
 */
- (NSInteger)numberOfItemsInSection:(NSInteger)sectionIndex;

/**
 Returns the section info object for the given section.
 */
- (id)sectionInfoForSection:(NSInteger)sectionIndex;

/**
 Returns the item at given index path.
 */
- (id)itemAtIndexPath:(NSIndexPath *)indexPath;

/**
 Returns the index path of the given object
 */
- (NSIndexPath *)indexPathForItem:(id)item;

@end

typedef NS_ENUM(NSUInteger, IDDataSourceChangeType) {
    IDDataSourceChangeInsert = 1,
    IDDataSourceChangeDelete = 2,
    IDDataSourceChangeMove = 3,
    IDDataSourceChangeUpdate = 4
};

@protocol IDDataSourceDelegate <NSObject>

/**
 Notifies the delegate that a fetched object has been changed due to an add, remove, move, or update.
	dataSource - controller instance that noticed the change on its fetched objects
	anObject - changed object
	indexPath - indexPath of changed object (nil for inserts)
	type - indicates if the change was an insert, delete, move, or update
	newIndexPath - the destination path for inserted or moved objects, nil otherwise
	
	Changes are reported with the following heuristics:
 
	On Adds and Removes, only the Added/Removed object is reported. It's assumed that all objects that come after the affected object are also moved, but these moves are not reported.
	The Move object is reported when the changed attribute on the object is one of the sort descriptors used in the fetch request.  An update of the object is assumed in this case, but no separate update message is sent to the delegate.
	The Update object is reported when an object's state changes, and the changed attributes aren't part of the sort keys.
 */
@optional
- (void)dataSource:(id<IDDataSource>)dataSource didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(IDDataSourceChangeType)type newIndexPath:(NSIndexPath *)newIndexPath;

/**
 Notifies the delegate of added or removed sections.
 
	dataSource - controller instance that noticed the change on its sections
	sectionInfo - changed section
	index - index of changed section
	type - indicates if the change was an insert or delete
 
	Changes on section info are reported before changes on fetchedObjects.
 */
@optional
- (void)dataSource:(id<IDDataSource>)dataSource didChangeSection:(id)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(IDDataSourceChangeType)type;

/**
 Notifies the delegate that section and object changes are about to be processed and notifications will be sent.
 Clients utilizing a UITableView may prepare for a batch of updates by responding to this method with -beginUpdates
 */
@optional
- (void)dataSourceWillChangeContent:(id<IDDataSource>)dataSource;

/**
 Notifies the delegate that all section and object changes have been sent.
 */
@optional
- (void)dataSourceDidChangeContent:(id<IDDataSource>)dataSource;

@end