# IDDataSource

[![CI Status](http://img.shields.io/travis/Ivan Damjanović/IDDataSource.svg?style=flat)](https://travis-ci.org/Ivan Damjanović/IDDataSource)
[![Version](https://img.shields.io/cocoapods/v/IDDataSource.svg?style=flat)](http://cocoapods.org/pods/IDDataSource)
[![License](https://img.shields.io/cocoapods/l/IDDataSource.svg?style=flat)](http://cocoapods.org/pods/IDDataSource)
[![Platform](https://img.shields.io/cocoapods/p/IDDataSource.svg?style=flat)](http://cocoapods.org/pods/IDDataSource)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

IDDataSource is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "IDDataSource"
```

## Author

Ivan Damjanović, ivan.damjanovic@infinum.hr

## License

IDDataSource is available under the MIT license. See the LICENSE file for more info.
